//
//  ServiceResponse.swift
//  grupoNach
//
//  Created by luis hector hernandez godinez  on 11/04/22.
//

import Foundation


struct ServiceResponse: Decodable{

    var colors: [String]
    var questions: [Questions]
}

struct Questions: Decodable{
    var total: Int
    var text: String
    var chartData: [ChartData]
}

struct ChartData: Decodable{
    var text: String
    var percetnage: Int
}

//
//  MenuMenuConfigurator.swift
//  grupoNach
//
//  Created by luis hector hernandez godinez  on 11/04/2022.
//  Copyright © 2022 n. All rights reserved.
//

import UIKit

class MenuModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? MenuViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: MenuViewController) {

        let router = MenuRouter()
        router.viewController = viewController

        let presenter = MenuPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = MenuInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}

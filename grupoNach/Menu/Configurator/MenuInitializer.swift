//
//  MenuMenuInitializer.swift
//  grupoNach
//
//  Created by luis hector hernandez godinez  on 11/04/2022.
//  Copyright © 2022 n. All rights reserved.
//

import UIKit

class MenuModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var menuViewController: MenuViewController!

    override func awakeFromNib() {

        let configurator = MenuModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: menuViewController)
    }

}

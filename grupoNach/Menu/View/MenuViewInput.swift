//
//  MenuMenuViewInput.swift
//  grupoNach
//
//  Created by luis hector hernandez godinez  on 11/04/2022.
//  Copyright © 2022 n. All rights reserved.
//

protocol MenuViewInput: class {

    /**
        @author luis hector hernandez godinez 
        Setup initial state of the view
    */
    func setupInitialState()
    func fetchSuccess(_ response: ServiceResponse)
    func fetchFailure(_ error: Error)

}

//
//  MenuMenuViewOutput.swift
//  grupoNach
//
//  Created by luis hector hernandez godinez  on 11/04/2022.
//  Copyright © 2022 n. All rights reserved.
//

protocol MenuViewOutput {

    /**
        @author luis hector hernandez godinez 
        Notify presenter that view is ready
    */

    func viewIsReady()
}

//
//  MenuMenuViewController.swift
//  grupoNach
//
//  Created by luis hector hernandez godinez  on 11/04/2022.
//  Copyright © 2022 n. All rights reserved.
//

import UIKit

enum ImageSource {
    case photoLibrary
    case camera
}

class MenuViewController: UIViewController, UIImagePickerControllerDelegate{


    @IBOutlet var mainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var output: MenuViewOutput!
    var dataService: ServiceResponse!
    var serviceManager = ServiceManager()
    var imagePicker: UIImagePickerController!
    var imagen: UIImage?
    var imagePath: String?


    @IBAction func upload(_ sender: UIButton) {

    }

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        output.viewIsReady()
        //loadColorFromFirebase()

        //refreshData()
    }

    func setup(){
        tableView.register(UINib(nibName: "NombreTableViewCell", bundle: nil), forCellReuseIdentifier: "NombreTableViewCell")
        tableView.register(UINib(nibName: "perfilTableViewCell", bundle: nil), forCellReuseIdentifier: "perfilTableViewCell")
        tableView.register(UINib(nibName: "GraficaTableViewCell", bundle: nil), forCellReuseIdentifier: "GraficaTableViewCell")
        tableView.register(UINib(nibName: "EmpresaTableViewCell", bundle: nil), forCellReuseIdentifier: "EmpresaTableViewCell")
        tableView.register(UINib(nibName: "TiendaTableViewCell", bundle: nil), forCellReuseIdentifier: "TiendaTableViewCell")
        tableView.register(UINib(nibName: "PromocionTableViewCell", bundle: nil), forCellReuseIdentifier: "PromocionTableViewCell")
        tableView.register(UINib(nibName: "ProductosTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductosTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
    }

    // MARK: HomeViewInput
    func setupInitialState() {
    }
}

extension MenuViewController: MenuViewInput{
    func fetchSuccess(_ response: ServiceResponse) {
        self.dataService = response

        tableView.reloadData()
    }

    func fetchFailure(_ error: Error) {

    }

}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == 0  {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "NombreTableViewCell") as? NombreTableViewCell
            {
                cell.txtName.placeholder = "Nombre"
                return cell
            }
        }

        else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "perfilTableViewCell") as! perfilTableViewCell
            cell.btnCharger.setTitle("Subir imagen", for: .normal)
            let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(captureImage))
            cell.btnCharger.addGestureRecognizer(imageTapGesture)
            if let image = self.imagen{
                cell.profileView.image = image
            }
            return cell
        }

        else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "GraficaTableViewCell") as! GraficaTableViewCell
            if (dataService != nil){
                debugPrint(dataService ?? "")
                let elements = dataService.questions[0].chartData

                cell.septup(dataResponse: dataService, elements: elements, index: indexPath, position: 0)
            }

            return cell
        }

        else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmpresaTableViewCell") as! EmpresaTableViewCell
            if (dataService != nil){
                debugPrint(dataService ?? "")
                let elements = dataService.questions[1].chartData

                cell.septup(dataResponse: dataService, elements: elements, index: indexPath, position: 1)
            }

            return cell
        }

        else if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TiendaTableViewCell") as! TiendaTableViewCell
            if (dataService != nil){
                debugPrint(dataService ?? "")
                let elements = dataService.questions[2].chartData

                cell.septup(dataResponse: dataService, elements: elements, index: indexPath, position: 2)
            }

            return cell
        }

        else if indexPath.row == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PromocionTableViewCell") as! PromocionTableViewCell
            if (dataService != nil){
                debugPrint(dataService ?? "")
                let elements = dataService.questions[3].chartData

                cell.septup(dataResponse: dataService, elements: elements, index: indexPath, position: 3)
            }

            return cell
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductosTableViewCell") as! ProductosTableViewCell
        if (dataService != nil){
            debugPrint(dataService ?? "")
            let elements = dataService.questions[4].chartData
            cell.septup(dataResponse: dataService, elements: elements, index: indexPath, position: 4)
        }

        return cell

    }

}

extension MenuViewController: UINavigationControllerDelegate{

    @objc func captureImage(_ sender: UIButton) {

        guard UIImagePickerController.isSourceTypeAvailable(.camera)  else {
            let alert = UIAlertController(title: "No camera", message: "This device does not support camera.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
            return
        }

        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        imagePicker.cameraCaptureMode = .photo
        present(imagePicker, animated: true, completion: nil)
    }



    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {

            print("image: \(image)")
            self.imagen = image
            self.tableView.reloadData()

        }

        if (picker.sourceType == UIImagePickerController.SourceType.camera) {

            let imgName = UUID().uuidString
            debugPrint("path image: \(imgName)")
            let documentDirectory = NSTemporaryDirectory()
            let localPath = documentDirectory.appending(imgName)


            let assetPath = info[.imageURL] as? URL
            if ((assetPath?.absoluteString.hasSuffix("JPG")) != nil) {
                    print("JPG")
                }
            else if ((assetPath?.absoluteString.hasSuffix("PNG")) != nil) {
                    print("PNG")
                }
            else if ((assetPath?.absoluteString.hasSuffix("GIF")) != nil) {
                    print("GIF")
                }
            else if ((assetPath?.absoluteString.hasSuffix("GIF")) != nil) {
                    print("JPEG")
                }
                else {
                    print("Unknown")
                }


        }




        dismiss(animated: true)

    }
}

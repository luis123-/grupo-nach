//
//  MenuMenuInteractor.swift
//  grupoNach
//
//  Created by luis hector hernandez godinez  on 11/04/2022.
//  Copyright © 2022 n. All rights reserved.
//

class MenuInteractor: MenuInteractorInput {

    weak var output: MenuInteractorOutput!
    var serviceManager = ServiceManager()

    func fetchData() {
        serviceManager.fetchDeta(){ result in
            switch result{
            case .success(let response):
                self.output.fetchSuccess(response)
            case .failure(let error):
                self.output.fetchFailure(error)
            }
        }
    }
}


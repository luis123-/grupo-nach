//
//  MenuMenuInteractorOutput.swift
//  grupoNach
//
//  Created by luis hector hernandez godinez  on 11/04/2022.
//  Copyright © 2022 n. All rights reserved.
//

import Foundation

protocol MenuInteractorOutput: class {
    func fetchSuccess(_ response: ServiceResponse)
    func fetchFailure(_ error: Error)
}

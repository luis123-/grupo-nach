//
//  GraficaTableViewCell.swift
//  grupoNach
//
//  Created by luis hector hernandez godinez  on 11/04/22.
//

import UIKit
import Charts

class GraficaTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var graficaView: PieChartView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func septup(dataResponse: ServiceResponse,elements: [ChartData], index: IndexPath, position: Int){
        self.lblTitle.text = dataResponse.questions[position].text
        var dataText: [String] = []
        var datapercetnage: [Int] = []
        for datas in elements {
            datapercetnage.append(datas.percetnage)
            dataText.append(datas.text)
        }

        customizeChart(dataPoints: dataText, values: datapercetnage.map{ Double($0) })
    }

    func customizeChart(dataPoints: [String], values: [Double]) {

        var dataEntries: [ChartDataEntry] = []
        for i in 0..<dataPoints.count {
            let dataEntry = PieChartDataEntry(value: values[i], label: dataPoints[i], data: dataPoints[i] as AnyObject)
            dataEntries.append(dataEntry)
        }

        let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: "")
        pieChartDataSet.colors = colorsOfCharts(numbersOfColor: dataPoints.count)

        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        let format = NumberFormatter()
        format.numberStyle = .none
        let formatter = DefaultValueFormatter(formatter: format)
        pieChartData.setValueFormatter(formatter)
        // 4. Assign it to the chart’s data
        graficaView.data = pieChartData
    }

    private func colorsOfCharts(numbersOfColor: Int) -> [UIColor] {
        var colors: [UIColor] = []
        for _ in 0..<numbersOfColor {
            let red = Double(arc4random_uniform(256))
            let green = Double(arc4random_uniform(256))
            let blue = Double(arc4random_uniform(256))
            let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
            colors.append(color)
        }
        return colors
    }

}

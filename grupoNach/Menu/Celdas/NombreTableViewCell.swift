//
//  NombreTableViewCell.swift
//  grupoNach
//
//  Created by luis hector hernandez godinez  on 11/04/22.
//

import UIKit
import Charts

class NombreTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtName: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

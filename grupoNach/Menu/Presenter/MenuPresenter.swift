//
//  MenuMenuPresenter.swift
//  grupoNach
//
//  Created by luis hector hernandez godinez  on 11/04/2022.
//  Copyright © 2022 n. All rights reserved.
//

class MenuPresenter: MenuModuleInput{

    weak var view: MenuViewInput!
    var interactor: MenuInteractorInput!
    var router: MenuRouterInput!

    func viewIsReady() {
        interactor.fetchData()
    }
}
extension MenuPresenter: MenuViewOutput{
}
extension MenuPresenter: MenuInteractorOutput{
    func fetchSuccess(_ response: ServiceResponse) {
        view.fetchSuccess(response)
    }

    func fetchFailure(_ error: Error) {
        view.fetchFailure(error)
    }
}

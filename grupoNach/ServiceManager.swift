//
//  ServiceManager.swift
//  grupoNach
//
//  Created by luis hector hernandez godinez  on 11/04/22.
//

import Foundation

class ServiceManager {

    static let shared = ServiceManager()
    var api_key = "12e0eb93808be66e1f4adde952ff6444"

    public init() {

    }

    func fetchDeta(closure: @escaping(Result<ServiceResponse, Error>) -> Void){
        //https://api.themoviedb.org/3/movie/634649?api_key=12e0eb93808be66e1f4adde952ff6444&language=en-US

        let session = URLSession.shared
        let url = URL(string: "https://us-central1-bibliotecadecontenido.cloudfunctions.net/helloWorld")!

        var request = URLRequest(url: url)
        request.httpMethod = "GET"

        let task = session.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                if let data = data {
                    do {

                        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                        print(json!)
                        let decoder = JSONDecoder()
                        let res = try decoder.decode(ServiceResponse.self, from: data)
                        closure(.success(res))
                        print(res)
                    } catch {
                        closure(.failure(error))
                        print("JSON error: \(error.localizedDescription)")
                    }
                }
            }
        }

        task.resume()
    }

}
